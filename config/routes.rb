Rails.application.routes.draw do
 
  get '/offers/:id/:choice' => 'offers#show'

  resources :offers
  resources :comments
  resources :administrations
  resources :games
  resources :dashboards
  resources :sessions
  resources :users
  resources :forums
  resources :ratings
 
  root 'sessions#new'

end
