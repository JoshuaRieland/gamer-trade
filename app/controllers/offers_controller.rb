class OffersController < ApplicationController
  def new
  	@offerRequirements = Game.find_by(id: params[:game_id])
  end

  def create
  	@offer = OfferUp.create(user_id: session[:user_id], game_id: params[:offer_game_id], game_title: params[:offer_title], game_genre: params[:offer_genre], game_comment: params[:offer_comment], trade_offer: params[:offer_console], trade_status: params[:trade_status], sale_offer: params[:offer_price], sale_status: params[:sale_status])
  	redirect_to '/games/index'
  end

  def show
    @choice = params[:choice]
    @game_id = params[:id]
    @game = Game.find(params[:id])
    if params[:choice] == 'Trade' then 
      @offers = OfferUp.all.where(game_id: params[:id], trade_status: 'Yes')
    end
    if params[:choice] == 'Sale' then
      @offers = OfferUp.all.where(game_id: params[:id], sale_status: 'Yes')
    end
  end

  def delete
  end
end
