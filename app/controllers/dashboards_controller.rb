class DashboardsController < ApplicationController
  def index
    @myInfo = User.find(session[:user_id])
    @myTrades = Game.all.where(user_id: session[:user_id], trade_status: 'Yes')
    @TradeOffers = []
    @offerTradeCount = 0
    
    @myTrades.each do |trade|
      @offerTradeCount = @offerTradeCount+1
      @TradeOffers[@offerTradeCount] = Offer.all.where(game_id: trade.id)
    end
    @mySales = Game.all.where(user_id: session[:user_id], sell_status: 'Yes')
    @SaleOffers = []
    @offerSaleCount = 0
   
    @mySales.each do |sale|
      @offerSaleCount = @offerSaleCount+1
      @SaleOffers[@offerSaleCount] = Offer.all.where(game_id: sale.id)
    end

  end

  def new
   redirect_to '/dashboards'
  end

  def create
  end

  def edit
  end

  def update
  end

  def show
  end

  def delete
  end
end
