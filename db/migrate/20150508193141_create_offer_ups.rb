class CreateOfferUps < ActiveRecord::Migration
  def change
    create_table :offer_ups do |t|
      t.integer :user_id
      t.integer :game_id
      t.string :game_title
      t.string :game_genre
      t.text :game_comment
      t.string :trade_offer
      t.string :trade_status
      t.float :sale_offer
      t.string :sale_status

      t.timestamps
    end
  end
end
